package demo.co.testScripts;

import java.io.FileNotFoundException;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.agiletestware.pangolin.annotations.Pangolin;
import demo.co.Base.BaseClass;
import demo.co.Base.CommonFunctions;
import demo.co.Config.Constants;
import demo.co.pageObjects.ChannelsPage;
import demo.co.pageObjects.HomePage;
import demo.co.pageObjects.LoginPage;
/**
 * @author Ranjit
 */

@Pangolin(sectionPath = "Master\\Section\\WebDriver")
public class ChannelsTestSuite extends BaseClass {
	
	public ChannelsTestSuite() {
		super();
	}
	
//	@Test(priority = 1)
//	public void EmpCanPostC10() throws FileNotFoundException, InterruptedException {
//		String randomString = demo.co.Base.CommonFunctions.generateRandomString(8);
//		LoginPage loginPage = new LoginPage();
//		loginPage.getLoginFunctionality(prop.getProperty("Employee1UserName"),
//				prop.getProperty("Employee1Password"));
//		demo.co.pageObjects.HomePage homePage = new demo.co.pageObjects.HomePage();
//		homePage.buttonSideMenu.click();
//		CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
//		ChannelsTestSuite channelsPage = new ChannelsTestSuite();
//		channelsPage.getFirstChannel().gettext();
//		channelsPage.getFirstChannel().click();
//		Assert.assertEquals(channelName, channelsPage.getTextChannelHeader().getText());
//		channelsPage.getButtonPencil().click();
//		Assert.assertEquals(channelsPage.getTextCreateAPost().getText(), Constants.CREATE_POST);
//		channelsPage.getTextAreaEditPost().sendKeys(randomString);
//		channelsPage.getButtonSend().click();
//		CommonFunctions.scrollToElementUsingText(driver, randomString);
//		Assert.assertEquals(channelsPage.getTextPostName().get(0).getText(), randomString);
//
//	}

	@Test(priority = 2)
	public void EmpCanDeleteC11() throws FileNotFoundException, InterruptedException {
		LoginPage loginPage = new LoginPage();
		String randomString = CommonFunctions.generateRandomString(8);
		loginPage.getLoginFunctionality(prop.getProperty("Employee1UserName"),
				prop.getProperty("Employee1Password"));
		HomePage homePage = new HomePage();
		Thread.sleep(5000);
		homePage.buttonSideMenu.click();
//		Thread.sleep(5000);
//		//CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
//		ChannelsPage channelsPage = new ChannelsPage();
//		channelsPage.buttonChannel.click();
//		Thread.sleep(5000);
//		channelsPage.buttonPencil.click();
//		Thread.sleep(5000);
//		channelsPage.textAreaEditPost.sendKeys(randomString);
//		Thread.sleep(5000);
//		channelsPage.buttonSend.click();
//		Thread.sleep(5000);
//		//CommonFunctions.scrollToElementUsingText(driver, randomString);
//		//Assert.assertEquals(channelsPage.textPostNames.get(0).getText(), randomString);
//		channelsPage.buttonPostEditMenus.get(0).click();
//		//Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).getText(),
//				Constants.DELETE_POST);
//		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
//				Constants.EDIT_POST);
//		System.out.println(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).isEnabled());
//		CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).click();
//		Assert.assertNotEquals(channelsPage.textPostNames.get(0).getText(), randomString);

	}

//	@Test(priority = 3)
//	public void EmpCanEditC12() throws FileNotFoundException, InterruptedException {
//		LoginPage loginPage = new LoginPage();
//		String randomString = CommonFunctions.generateRandomString(8);
//		String randomString2 = CommonFunctions.generateRandomString(8);
//		loginPage.getLoginFunctionality(prop.getProperty("Employee1UserName"),
//				prop.getProperty("Employee1Password"));
//		HomePage homePage = new HomePage();
//		homePage.buttonSideMenu.click();
//		CommonFunctions.selectElementUsingText(Constants.CHANNELS, getDriver()).click();
//		ChannelsPage channelsPage = new ChannelsPage();
//		channelsPage.buttonChannel.click();
//		channelsPage.buttonPencil.click();
//		channelsPage.textAreaEditPost.sendKeys(randomString);
//		channelsPage.buttonSend.click();
//		//CommonFunctions.scrollToElementUsingText(driver, randomString);
//		Assert.assertEquals(channelsPage.textPostNames.get(0).getText(), randomString);
//		channelsPage.buttonPostEditMenus.get(0).click();
//		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.DELETE_POST, getDriver()).getText(),
//				Constants.DELETE_POST);
//		Assert.assertEquals(CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).getText(),
//				Constants.EDIT_POST);
//		CommonFunctions.selectElementUsingText(Constants.EDIT_POST, getDriver()).click();
//		Assert.assertEquals(channelsPage.headerEditPost.getText(), Constants.EDIT_POST);
//		channelsPage.textAreaEditPost.sendKeys(randomString2);
//		System.out.println(channelsPage.textAreaEditPost.isEnabled());
//		channelsPage.buttonSend.click();
//		Thread.sleep(6000);
//	//	driver.navigate().back();
//		channelsPage.buttonChannel.click();
//	//	CommonFunctions.scrollToElementUsingText(driver, randomString2);
//		Assert.assertEquals(channelsPage.textPostNames.get(0).getText(), randomString2);
//
//	}
	

}
