package demo.co.pageObjects;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import demo.co.Base.BaseClass;

/**
 * @author Ranjit
 */
public class ChannelsPage extends BaseClass {

	public ChannelsPage() {
		PageFactory.initElements(getDriver(), this);
	}

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ImageButton")
	public WebElement buttonPencil;

	@FindBy(id = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout[1]/androidx.recyclerview.widget.RecyclerView/androidx.appcompat.widget.LinearLayoutCompat[7]/android.widget.CheckedTextView")
	public WebElement buttonChannel;

	@FindBy(xpath = "//*[@text='Create a post']")
	public WebElement textCreateAPost;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.EditText")
	public WebElement textAreaEditPost;

//	@FindBy(xpath = "//*[@resource-id='com.workjam.workjam.qa:id/channel_post_content_text_view']")
//	public ArrayList<WebElement> textPostNames;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView")
	public WebElement textChannelHeader;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")
	public WebElement textChannelName;

	@FindBy(xpath = "//android.widget.TextView[@content-desc='Send']")
	public WebElement buttonSend;

	@FindBy(xpath = "//*[@resource-id='com.workjam.workjam.qa:id/channel_post_edit_menu']")
	public ArrayList<WebElement> buttonPostEditMenus;

	@FindBy(xpath = "//*[@text='Edit post']")
	public WebElement headerEditPost;

}
