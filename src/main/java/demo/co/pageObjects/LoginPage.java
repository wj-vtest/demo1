package demo.co.pageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import demo.co.Base.BaseClass;

/**
 * @author Ranjit
 */
public class LoginPage extends BaseClass {


	public LoginPage() {
		PageFactory.initElements(getDriver(), this);
	}

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
	public WebElement textBoxUserName;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.EditText")
	public WebElement textBoxPassword;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")
	public WebElement buttonContinue;

	@FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]")
	public WebElement buttonLogin;

	public void getLoginFunctionality(String username, String password) throws InterruptedException {
		LoginPage loginPage = new LoginPage();
		Thread.sleep(6000);
		loginPage.textBoxUserName.sendKeys(username);
		Thread.sleep(8000);
		loginPage.buttonContinue.click();
		Thread.sleep(8000);
		loginPage.textBoxPassword.sendKeys(password);
		Thread.sleep(8000);
		loginPage.buttonLogin.click();
		Thread.sleep(8000);
//		HomePage homePage = new HomePage();
//		String[] expected = username.toUpperCase().split("@");
//		String[] actual = homePage.header.getText().split(" ");
//		if (actual[1].contains(expected[0])) {
//			System.out.println("Login is successful");
		}

	}


