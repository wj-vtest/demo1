package demo.co.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import demo.co.Base.BaseClass;

public class SurveyPage extends BaseClass {

	WebDriver driver;

	public SurveyPage() {
		PageFactory.initElements(getDriver(), this);
	}
	@FindBy(name = "username")
	public WebElement username;

	@FindBy(xpath = "//button[text()='Continue']")
	public WebElement continueButton;

	@FindBy(name = "password")
	public WebElement password;

	@FindBy(xpath = "//button[text()='Log In']")
	public WebElement logInButton;

	@FindBy(xpath = "//span[text()='Surveys']")
	public WebElement surveyLink;

	@FindBy(xpath = "//button[text()='Add survey']")
	public WebElement addSurveyButton;

	@FindBy(id = "surveyName-")
	public WebElement surveyName;

	@FindBy(xpath = "//select[@id='type']")
	public WebElement surveyType;

	@FindBy(id = "surveyBadges")
	public WebElement surveyBadges;

	@FindBy(id = "segmentationList")
	public WebElement targetAudience;

	@FindBy(id = "surveyStatus")
	public WebElement surveyStatus;

	@FindBy(xpath = "//span[text()='Add question']")
	public WebElement addQuestion;

	@FindBy(id = "questionType")
	public WebElement questionType;

	@FindBy(xpath = "//input[@name='question-' and @required='required']")
	public WebElement question;

	@FindBy(xpath = "//input[@value='Save']")
	public WebElement saveButton;

	@FindBy(xpath = "//span[text()='Add answer']")
	public WebElement addAnswer;

	@FindBy(xpath = "//input[@value='Create survey']")
	public WebElement createSurveyButton;

}
