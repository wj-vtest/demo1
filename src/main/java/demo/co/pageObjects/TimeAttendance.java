package demo.co.pageObjects;

import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import demo.co.Base.BaseClass;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * @author Bobby
 */
public class TimeAttendance extends BaseClass {

	public TimeAttendance() {
		PageFactory.initElements(getDriver(), this);
	}

	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
	public WebElement menu;

	@AndroidFindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout[1]/androidx.recyclerview.widget.RecyclerView/androidx.appcompat.widget.LinearLayoutCompat[4]/android.widget.CheckedTextView\n"
			+ "")
	public WebElement tna1;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_shift_start_checked_text_view")
	public WebElement clockin;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_shift_end_checked_text_view")
	public WebElement clockout;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_break_start_checked_text_view")
	public WebElement breakin;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_break_end_checked_text_view")
	public WebElement breakout;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_meal_start_checked_text_view")
	public WebElement mealin;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_meal_end_checked_text_view")
	public WebElement mealout;

	@AndroidFindBy(id = "com.workjam.workjam.qa:id/punch_clock_punch_button")
	public WebElement punch;

	@AndroidFindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout")
	public WebElement popup;

	@AndroidFindBy(xpath = "//androidx.appcompat.app.a.c[@content-desc=\"Timecards\"]")
	public WebElement timecards;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView[1]")
	public List<WebElement> currentDateRange;

	@AndroidFindAll(@AndroidBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]"))
	public List<WebElement> currentDate;

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.viewpager.widget.ViewPager/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]")
	public List<WebElement> timeRecords;
}
