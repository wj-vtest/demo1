package demo.co.Config;

/**
 * @author Ranjit
 */
public class Constants {

	public static final String PLATFORM_NAME = "Android";
	public static final String PLATFORM_VERSION = "9.0";// "7.1.1";
	public static final String DEVICE_NAME = "192.168.148.102:5555";// "ZY223HQBHZ";
	public static final String APP_PACKAGE = "com.workjam.workjam.qa";
	public static final String APP_ACTIVITY = "com.workjam.workjam.features.authentication.LoginUsernameActivity";
	public static final String URL = "http://127.0.0.1:4723/wd/hub";
	public static final String CREDENTIAL_FILE_PATH = System.getProperty("user.dir")
			+ "//demo//src//main//java//demo//co//Config//credentials.properties";
	public static final String CREATE_POST = "Create a post";
	public static final String EDIT_POST = "Edit post";
	public static final String DELETE_POST = "Delete post";
	public static final String CHANNELS = "Channels";
	public static final String TIME_ATTENDANCE = "Time & attendance";
	public static final String SURVEYS = "Surveys";
}
